# Simple State Machine

## Introduction

A simple state machine, able to set up conditions and transitions.

## Code Samples

> StateMachine.cs  [Inherit from this class and overwrite the initialization]

## Installation

public class ExampleStateMachine : StateMachine
{
    public override void Initialize(bool autoRun = true)
    {
        shouldAutoRun = autoRun;

        IState stateOne = new State();
        stateOne.StateName = "Testing State One";

        IState stateTwo = new State();
        stateTwo.StateName = "Testing state Two";

        RuleSet rulesOne = new RuleSet(new List<Func<bool>>()
        {
            StateOneCompleted
        });

        RuleSet rulesTwo = new RuleSet(new List<Func<bool>>()
        {
            StateTwoCompleted
        });

        stateOne.StateTransition(rulesOne, () => SwitchState(stateTwo));       
        stateTwo.StateTransition(rulesTwo, () => SwitchState(stateOne));

        SetEntryPoint(stateOne, autoRun);
    }

    public override void UpdateLoop()
    {
        base.UpdateLoop();

        if (Input.GetKeyDown(KeyCode.Space))
        {
            shouldAutoRun = true;
        }
    }
    private bool StateOneCompleted()
    {
        return Input.GetKeyDown(KeyCode.Q);
    }

    private bool StateTwoCompleted()
    {
        return Input.GetKeyDown(KeyCode.W);
    }