﻿using System;
using SimpleStateMachine.Rules;

namespace SimpleStateMachine.States
{
    public interface IState
    {
        void ClearAllMethods();
        void SetDebugging(bool shouldDebug);
        void UpdateState();
        void StateTransition(RuleSet rules, Action callback);

        void AddStartMethod(Action action);
        void AddUpdateMethod(Action action);
        void AddExitMethod(Action action);

        void EnvokeStartMethods();
        void EnvokeUpdateMethods();
        void EnvokeExitMethods();

        string StateName { get; set; }
    }
}
