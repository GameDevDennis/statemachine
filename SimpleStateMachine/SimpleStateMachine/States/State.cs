﻿using SimpleStateMachine.Rules;
using System;
using System.Collections.Generic;

namespace SimpleStateMachine.States
{
    public class State : IState
    {
        private List<Action> startActionList = new List<Action>();
        private List<Action> updateActionList = new List<Action>();
        private List<Action> exitActionList = new List<Action>();

        RuleSet rules = null;
        Action conditionMetCallBack;

        private bool isDebugging = false;

        public string StateName { get; set; }

        #region Adding Methods
        public void AddExitMethod(Action action)
        {
            startActionList.Add(action);
        }

        public void AddStartMethod(Action action)
        {
            updateActionList.Add(action);
        }

        public void AddUpdateMethod(Action action)
        {
            exitActionList.Add(action);
        }

        #endregion

        /// <summary>
        /// Responisble for reseting all action lists. 
        /// </summary>
        public void ClearAllMethods()
        {
            startActionList = new List<Action>();
            updateActionList = new List<Action>();
            exitActionList = new List<Action>();
        }

        #region Envoke Methods
        public void EnvokeExitMethods()
        {
            for (int i = 0; i < exitActionList.Count; i++)
            {
                exitActionList[i].Invoke();
            }
        }

        public void EnvokeStartMethods()
        {
            for (int i = 0; i < startActionList.Count; i++)
            {
                startActionList[i].Invoke();
            }
        }

        public void EnvokeUpdateMethods()
        {
            for (int i = 0; i < updateActionList.Count; i++)
            {
                updateActionList[i].Invoke();
            }
        }
        #endregion

        public void SetDebugging(bool shouldDebug)
        {
            isDebugging = shouldDebug;
        }

        /// <summary>
        /// Set the rules, and callback when all rules are met.
        /// </summary>
        /// <param name="ruleSet">List of rules which have to be met.</param>
        /// <param name="changeState"></param>
        public void StateTransition(RuleSet ruleSet, Action changeState)
        {
            rules = ruleSet;
            conditionMetCallBack = changeState;
        }

        /// <summary>
        /// Core update loop, runs on the Unity Update loop.
        /// </summary>
        public void UpdateState()
        {
            EnvokeUpdateMethods();
            RuleChecker();
        }

        /// <summary>
        /// Check if all conditions are met for this state.
        /// </summary>
        private void RuleChecker()
        {
            if (rules != null)
            {
                if (rules.ConditionsMet())
                {
                    conditionMetCallBack.Invoke();
                }
            }
        }
    }
}
