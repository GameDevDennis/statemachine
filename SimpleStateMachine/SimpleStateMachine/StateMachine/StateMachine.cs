﻿using SimpleStateMachine.States;

namespace SimpleStateMachine
{
    public abstract class StateMachine
    {
        private IState currentState;
        public IState CurrentState { get; private set; }

        //Allows you to control if the statemachine should instantly run. Default value is True!
        public bool shouldAutoRun;

        /// <summary>
        /// Initialize states within this method. and make the transitions.
        /// </summary>
        /// <param name="autoRun">Should the method automatically start running.</param>
        public abstract void Initialize(bool autoRun = true);

        public virtual void UpdateLoop()
        {
            if (currentState != null && shouldAutoRun)
            {
                currentState.UpdateState();
            }
        }

        public void SwitchState(IState newState)
        {
            //State exits, all methods assigned get envoked.
            currentState.EnvokeExitMethods();
            //CurrentState wil be updated with the newState
            currentState = newState;
            //The new currentState will envoke all methods assigned.
            currentState.EnvokeStartMethods();
        }

        /// <summary>
        /// Changes the currentState
        /// </summary>
        /// <param name="goToState"></param>
        /// <param name="isForce">When forcing it no exit methods will get called.</param>
        public void GoToState(IState goToState, bool isForce = false)
        {
            if (isForce)
            {
                ForceStart(goToState);
            }
            else
            {
                SwitchState(goToState);
            }
        }

        /// <summary>
        /// Sets the point where the statemachine will start.
        /// </summary>
        /// <param name="entryPoint"></param>
        /// <param name="isAutoStart"></param>
        public void SetEntryPoint(IState entryPoint, bool isAutoStart)
        {
            if (isAutoStart)
            {
                ForceStart(entryPoint);
            }
            else
            {
                currentState = entryPoint;
            }
        }

        /// <summary>
        /// Stops the statemachine, update will no longer happen.
        /// </summary>
        /// <param name="forceQuit">When forced no exit methods will get executed</param>
        public void StopStateMachine(bool forceQuit = false)
        {
            if (forceQuit)
            {
                currentState = null;
            }
            else
            {
                currentState.EnvokeExitMethods();
                currentState = null;
            }
        }

        /// <summary>
        /// Start the state and envokes the starts methods only.
        /// </summary>
        /// <param name="state"></param>
        private void ForceStart(IState state)
        {
            currentState = state;
            state.EnvokeStartMethods();
        }
    }
}
