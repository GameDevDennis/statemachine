﻿using System;
using System.Collections.Generic;

namespace SimpleStateMachine.Rules
{
    public class RuleSet
    {
        private List<Func<bool>> rules;

        public RuleSet(List<Func<bool>> ruleSet)
        {
            rules = ruleSet;
        }

        public bool ConditionsMet()
        {
            for (int i = 0; i < rules.Count; i++)
            {
                if (!rules[i].Invoke())
                {
                    return false;
                }
            }

            return true;
        }
    }
}
