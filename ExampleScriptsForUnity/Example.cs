﻿using UnityEngine;
using System.Collections;
using SimpleStateMachine;

public class Example : MonoBehaviour
{
    StateMachine exampleMachine;

    private void Start()
    {
        exampleMachine = new ExampleStateMachine();
        exampleMachine.Initialize();
    }

    private void Update()
    {
        exampleMachine.UpdateLoop();
    }
}
